﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiDocker_Try1.Model
{
    public record BrowsingRecord
    {
        [Key]
        public int id { get; set; }
        public DateTime browsing_date { get; set; }
        public string browse_url { get; set; }
        public string tags { get; set; }
    }
}
