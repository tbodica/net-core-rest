﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiDocker_Try1.Model
{
    public record MappedClass_Test 
    {
        [Key]
        public int hands { get; set; }
        //public int ensure_created_automigrate_questionmark { get; set; } // Npgsql.PostgresException: '42703: column m.ensure_created_automigrate_questionmark does not exist'
        public string tattoo_name { get; set; }
    }

}
