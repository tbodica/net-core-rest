﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiDocker_Try1.Model;

namespace WebApiDocker_Try1.Context
{
    public class MappedTestContext : DbContext
    {

        public DbSet<MappedClass_Test> MappedTests { get; set; }
        private string PasswordString = "";

        public MappedTestContext(string password)
        {
            this.PasswordString = password;
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(String.Format("Host = {0}; Username = {1}; Password = {2}; Database = {3}",
                        "192.168.0.192",
                        "postgres",
                        this.PasswordString,
                        "random_test_db"
                ));
        }
    }
}
