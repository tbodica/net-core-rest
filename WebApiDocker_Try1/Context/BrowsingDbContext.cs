﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiDocker_Try1.Model;

namespace WebApiDocker_Try1.Context
{
    public class BrowsingDbContext : DbContext
    {
        public DbSet<BrowsingRecord> cell_testing { get; set; }
        private string PasswordString = "";

        public BrowsingDbContext(string password)
        {
            this.PasswordString = password;
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(String.Format("Host = {0}; Username = {1}; Password = {2}; Database = {3}",
                        "192.168.0.192",
                        "postgres",
                        this.PasswordString,
                        "browsing"
                ));
        }
    }
}
