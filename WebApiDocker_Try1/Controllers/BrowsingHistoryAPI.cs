﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using WebApiDocker_Try1.Context;
using WebApiDocker_Try1.Model;
using WebApiDocker_Try1.Configuration;
using WebApiDocker_Try1.Controllers.Model_RequestBody;

namespace WebApiDocker_Try1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BrowsingHistoryAPI
    {

        private readonly ILogger<BrowsingHistoryAPI> _logger;

        public BrowsingHistoryAPI(ILogger<BrowsingHistoryAPI> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<BrowsingRecord> Get(int count=0, [FromBody] Get_BrowsingRecord body = null)
        {
            var ConfigJsonString = System.IO.File.ReadAllText("db_config.json");
            var SqlConfigPOCO = JsonSerializer.Deserialize<SqlConfigPOCO>(ConfigJsonString.ToString());


            using (var db = new BrowsingDbContext(SqlConfigPOCO.password))
            {
                if (count != 0)
                    return db.cell_testing.ToList().Skip(0).Take(count).Select(x => x);

                if (body != null)
                    if (body.count != 0)
                        return db.cell_testing.ToList().Skip(0).Take(body.count).Select(x => x);

                return db.cell_testing.ToList();
            }
        }

    }
}
