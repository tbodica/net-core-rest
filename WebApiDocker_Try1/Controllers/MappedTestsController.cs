﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using WebApiDocker_Try1.Configuration;
using WebApiDocker_Try1.Context;
using WebApiDocker_Try1.Model;

namespace WebApiDocker_Try1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MappedTestsController
    {

        private static readonly string[] Summaries = new[]
        {
            "NotImpl", "Asdf."
        };

        private readonly ILogger<MappedTestsController> _logger;

        public MappedTestsController(ILogger<MappedTestsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<MappedClass_Test> Get()
        {

            var ConfigJsonString = System.IO.File.ReadAllText("db_config.json");
            var SqlConfigPOCO = JsonSerializer.Deserialize<SqlConfigPOCO>(ConfigJsonString.ToString());


            using (var db = new MappedTestContext(SqlConfigPOCO.password))
            {
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();

                var c = new MappedClass_Test { hands = 1, tattoo_name = "like" };
                db.Add(c);
                db.SaveChanges();
                
                return db.MappedTests.ToList();
            }
        }
    }
}
